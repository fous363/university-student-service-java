FROM gradle:jdk11-hotspot

WORKDIR .
COPY ./ ./
RUN gradle bootJar

CMD ["java","-jar","-Dspring.profiles.active=prod","./build/libs/student-app.jar"]