# University-Student-Service-Java

This the student service project.

Endpoints:

- GET /api/student/user/{userId} - get student by user id({userId})
- POST /api/student - create student
- GET /api/student/assigned/course/{courseId} - get students, that are assigned to course{courseId}
- POST /api/student/list/assign/course/{courseId} - assign students to course{courseId}
- GET /api/student/unassigned/course/{courseId} - get students, that are not assigned to course{courseId}
- POST /api/student/{id}/unassign/course/{courseId} - remove student{id} with course{id}
- GET /api/student?ids={ids}&exclude-ids={excludeIds} - if both query parameters don't have a value, then get all students; 
  if 'ids' query parameter have a value, then get students with specified ids. 
  If 'exclude-ids' query paramter, then get student without specified ids.
- GET /api/student/{id}/courses/id - get courses' id for the student{id}. 

For local environment run:

- Update the application-dev.yml
- Run "gradle bootRun --args="--spring.profiles.active=dev""

For production environment run:

- Update application-prod.yml
- Build from Dockerfile