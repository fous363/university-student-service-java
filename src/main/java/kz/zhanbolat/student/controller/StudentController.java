package kz.zhanbolat.student.controller;

import kz.zhanbolat.student.controller.dto.ErrorResponse;
import kz.zhanbolat.student.entity.Student;
import kz.zhanbolat.student.exception.ServiceException;
import kz.zhanbolat.student.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest controller for student
 */
@RestController
@RequestMapping("/api/student")
public class StudentController {
    private static final Logger logger = LoggerFactory.getLogger(StudentController.class);
    @Autowired
    private StudentService studentService;

    /**
     * Get a student by user's id
     *
     * @param userId - user' id
     * @return student
     */
    @GetMapping("/user/{userId}")
    public Student getStudentByUserId(@PathVariable("userId") Long userId) {
        return studentService.getStudentByUserId(userId);
    }

    /**
     * Create student. The request body must not have the id field as not null.
     *
     * @param student - student
     * @return created student
     */
    @PostMapping
    public Student addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    /**
     * Get students assigned to the course.
     *
     * @param courseId - course's id
     * @return list of students
     */
    @GetMapping("/assigned/course/{courseId}")
    public List<Student> getStudentsAssignedToCourse(@PathVariable("courseId") Long courseId) {
        return studentService.getStudentsAssignedToCourse(courseId);
    }

    /**
     * Assign students to course.
     *
     * @param courseId - course's id
     * @param students - list of students
     * @return response
     */
    @PostMapping("/list/assign/course/{courseId}")
    public List<Student> assignStudentListToCourse(@PathVariable("courseId") Long courseId,
                                                  @RequestBody List<Student> students) {
        studentService.assignStudentListToCourse(courseId, students);
        return students;
    }

    /**
     * Get students, that doesn't have the course.
     *
     * @param courseId - course's id.
     * @return list of students
     */
    @GetMapping("/unassigned/course/{courseId}")
    public List<Student> getStudentsUnassignedToCourse(@PathVariable("courseId") Long courseId) {
        return studentService.getStudentsUnassignedToCourse(courseId);
    }

    /**
     *  Remove course with student
     *
     * @param studentId - student's id
     * @param courseId - course's id
     */
    @DeleteMapping("{id}/remove/course/{courseId}")
    public void removeCourseFromStudent(@PathVariable("id") Long studentId,
                                                @PathVariable("courseId") Long courseId) {
        studentService.removeStudentFromCourse(studentId, courseId);
    }

    /**
     * Get students' list. If ids is not null, then return list of students with these ids.
     * If excludeIds is not null, then return list of students without these ids.
     * If they both are null, then return all students list.
     *
     * @param ids - students' id
     * @param excludeIds - students' id to exclude
     * @return list of students
     */
    @GetMapping
    public List<Student> getStudents(@RequestParam(name = "ids", required = false) Long[] ids,
                                     @RequestParam(name = "exclude-ids", required = false) Long[] excludeIds) {
        return studentService.getStudents(ids, excludeIds);
    }

    /**
     * Get courses' id of student.
     *
     * @param id - student's id
     * @return list of courses' id
     */
    @GetMapping("/{id}/courses/id")
    public List<Long> getCoursesId(@PathVariable("id") Long id) {
        return studentService.getCoursesId(id);
    }

    /**
     * Return with error response of internal server error
     *
     * @param exception - exception
     * @return Error response with 'internal server error' message
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler
    public ErrorResponse handleException(Exception exception) {
        logger.error("Caught exception: ", exception);
        return new ErrorResponse("Internal server error.");
    }

    /**
     * Return with error response with exception message
     *
     * @param exception - exception
     * @return error response with exception message
     *
     * @see ErrorResponse
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServiceException.class)
    public ErrorResponse handleServiceException(ServiceException exception) {
        logger.error("Caught service exception: ", exception);
        return new ErrorResponse(exception.getMessage());
    }
}
