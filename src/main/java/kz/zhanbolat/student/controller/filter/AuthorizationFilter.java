package kz.zhanbolat.student.controller.filter;

import kz.zhanbolat.student.controller.dto.AuthorizationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Authorization filter to create the context of the client to the client's permission
 */
@Component
public class AuthorizationFilter extends OncePerRequestFilter {
    @Value("${authorization.request.url}")
    private String authorizationRequestUrl;
    @Autowired
    private RestTemplate restTemplate;
    @Value("${app.env.test.enabled:false}")
    private boolean isTestEnv;

    /**
     * {@inheritDoc}
     *
     * Create the request to authorization server, and wrap the response with class <code>AuthorizationResponse</code>.
     * If user is authorized, then create context
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!isTestEnv && Objects.nonNull(request.getCookies())  && request.getCookies().length > 0) {
            String cookieHeader = Arrays.stream(request.getCookies())
                    .map(cookie -> cookie.getName() + "=" + cookie.getValue())
                    .collect(Collectors.joining("; "));
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", cookieHeader);
            try {
                ResponseEntity<AuthorizationResponse> responseEntity = restTemplate.exchange(authorizationRequestUrl,
                        HttpMethod.POST, new HttpEntity<>(null, requestHeaders), AuthorizationResponse.class);
                AuthorizationResponse authorizationResponse = responseEntity.getBody();
                if (authorizationResponse.isAuthorized()) {
                    UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                            authorizationResponse.getAuthorizationSubject(),
                            null,
                            mapToSimpleGrantedAuthorityList(authorizationResponse.getAuthorizationRoles()));
                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            } catch (HttpServerErrorException e) {
                logger.error("Authorization exception", e);
            }
        }
        filterChain.doFilter(request, response);
    }

    private List<SimpleGrantedAuthority> mapToSimpleGrantedAuthorityList(List<String> authorizationRoles) {
        return authorizationRoles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
