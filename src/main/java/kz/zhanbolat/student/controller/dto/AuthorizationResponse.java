package kz.zhanbolat.student.controller.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Authorization response from authorization server
 */
public class AuthorizationResponse {
    private boolean isAuthorized;
    private String authorizationSubject;
    private List<String> authorizationRoles = new ArrayList<>();

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        isAuthorized = authorized;
    }

    public String getAuthorizationSubject() {
        return authorizationSubject;
    }

    public void setAuthorizationSubject(String authorizationSubject) {
        this.authorizationSubject = authorizationSubject;
    }

    public List<String> getAuthorizationRoles() {
        return authorizationRoles;
    }

    public void setAuthorizationRoles(List<String> authorizationRoles) {
        this.authorizationRoles = authorizationRoles;
    }
}
