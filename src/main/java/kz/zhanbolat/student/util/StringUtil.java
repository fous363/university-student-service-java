package kz.zhanbolat.student.util;

import java.util.Objects;

public final class StringUtil {
    private static final String EMPTY_STRING = "";

    /**
     * Check if string is empty
     *
     * @param value - string
     * @return true, if string is null or empty, otherwise false
     */
    public static boolean isEmpty(String value) {
        return Objects.isNull(value) || EMPTY_STRING.equals(value);
    }

    /**
     * Check if string is empty. If it is, then throw exception with specified message
     *
     * @param value - string
     * @param msg - exception message
     *
     * @throws IllegalArgumentException
     */
    public static void requiredNotEmpty(String value, String msg) {
        if (isEmpty(value)) {
            throw new IllegalArgumentException(msg);
        }
    }
}
