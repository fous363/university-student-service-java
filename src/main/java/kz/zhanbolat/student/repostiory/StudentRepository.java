package kz.zhanbolat.student.repostiory;

import kz.zhanbolat.student.entity.Student;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The repository class for <code>Student</code> entity class
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    /**
     * Find student by its account id
     *
     * @param userId - account id
     * @return student
     */
    Optional<Student> findOneByUserId(Long userId);

    /**
     * Check if the student is already assigned to the course
     *
     * @param studentId - student's id
     * @param courseId - course's id
     * @return true, if student is assigned to the course, otherwise false
     */
    @Query(value = "select case when count(*) > 0 then true else false end " +
            "from student_course where student_id = ?1 and course_id = ?2", nativeQuery = true)
    boolean isStudentAlreadyAssignedToCourse(Long studentId, Long courseId);

    /**
     * Assign student to the course. REQUIRED transaction to perform
     *
     * @param studentId - student's id
     * @param courseId - course's id
     */
    @Modifying
    @Query(value = "insert into student_course(student_id, course_id) values (?1, ?2)", nativeQuery = true)
    void assignStudentCourse(Long studentId, Long courseId);

    /**
     * Find students by the course's id
     *
     * @param courseId - course's id
     * @return list of students
     */
    @Query(value = "select st.id, st.full_name, st.birthday, st.email, st.phone, st.user_id from student st " +
            "inner join student_course stc on st.id = stc.student_id and stc.course_id = ?1", nativeQuery = true)
    List<Student> findAllByCourseId(Long courseId);

    /**
     * Find students, that are not assigned to the course
     *
     * @param courseId - course's id
     * @return list of students
     */
    @Query(value = "select st.id, st.full_name, st.birthday, st.email, st.phone, st.user_id from student st " +
            "where st.id not in (select stc.student_id from student_course stc where stc.course_id = ?1)", nativeQuery = true)
    List<Student> findAllUnassignedToCourse(Long courseId);

    /**
     * Remove student with course. REQUIRED transaction to perform
     *
     * @param studentId - student's id
     * @param courseId - course's id
     */
    @Modifying
    @Query(value = "delete from student_course where student_id = ?1 and course_id = ?2", nativeQuery = true)
    void removeStudentFromCourse(Long studentId, Long courseId);

    /**
     * Find students, whose id is not in list of specified ids.
     *
     * @param ids - students' id to exclude
     * @return list of students
     */
    @Query(value = "select st.id, st.full_name, st.birthday, st.email, st.phone, st.user_id from student st " +
            "where st.id not in (?1)", nativeQuery = true)
    List<Student> findAllWithoutId(List<Long> ids);

    /**
     * Find courses of the student
     *
     * @param id - student's id
     * @return list of courses' id
     */
    @Query(value = "select course_id from student_course where student_id = ?1", nativeQuery = true)
    List<Long> findCoursesIdByStudentId(Long id);
}
