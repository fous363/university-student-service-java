package kz.zhanbolat.student.service;

import kz.zhanbolat.student.checker.EntityChecker;
import kz.zhanbolat.student.entity.Student;
import kz.zhanbolat.student.exception.ServiceException;
import kz.zhanbolat.student.repostiory.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Implementation class for <code>StudentService</code>
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private EntityChecker<Student> studentEntityChecker;

    /**
     * {@inheritDoc}
     *
     * Use studentRepository to find the student by account id
     *
     * @see StudentRepository
     *
     * @throws ServiceException
     * @throws NullPointerException
     */
    @Override
    public Student getStudentByUserId(Long userId) {
        Objects.requireNonNull(userId, "User id cannot be null.");
        return studentRepository.findOneByUserId(userId)
                .orElseThrow(() -> new ServiceException("Student with user id " + userId + " is not found."));
    }

    /**
     * {@inheritDoc}
     *
     * Use studentRepository to create the student
     *
     * @see StudentRepository
     *
     * @throws NullPointerException
     */
    @Override
    public Student addStudent(Student student) {
        studentEntityChecker.checkOnCreate(student);
        if (Objects.nonNull(student.getId()) && studentRepository.existsById(student.getId())) {
            throw new ServiceException("Student already exists");
        }
        return studentRepository.save(student);
    }

    /**
     * {@inheritDoc}
     *
     * Use studentRepository to find students by the course's id
     *
     * @see StudentRepository
     *
     * @throws NullPointerException
     */
    @Override
    public List<Student> getStudentsAssignedToCourse(Long courseId) {
        Objects.requireNonNull(courseId, "Course id cannot be null.");

        return studentRepository.findAllByCourseId(courseId);
    }

    /**
     * {@inheritDoc}
     *
     * Check students, if they are not assign to the course. If any of the them are assigned, then throw exception.
     * Otherwise all students wll be assigned to the course.
     *
     * @see StudentRepository
     * @see Transactional
     *
     * @throws NullPointerException
     * @throws IllegalArgumentException
     * @throws ServiceException
     */
    @Override
    @Transactional
    public void assignStudentListToCourse(Long courseId, List<Student> students) {
        Objects.requireNonNull(courseId, "Course id cannot be null.");
        Objects.requireNonNull(students, "Students cannot be null.");
        if (students.isEmpty()) {
            throw new IllegalArgumentException("Student list cannot be null.");
        }
        students.stream()
                .filter(student -> studentRepository.isStudentAlreadyAssignedToCourse(student.getId(), courseId))
                .findFirst()
                .ifPresent((student) -> { throw new ServiceException("Course is already assigned to student '"
                            + student.getFullName() + "'"); });

        students.forEach(student -> studentRepository.assignStudentCourse(student.getId(), courseId));
    }

    /**
     * {@inheritDoc}
     *
     * Use studentRepository to find students, that are not assigned to course
     *
     * @see StudentRepository
     *
     * @throws NullPointerException
     */
    @Override
    public List<Student> getStudentsUnassignedToCourse(Long courseId) {
        Objects.requireNonNull(courseId, "Course id cannot be null.");

        return studentRepository.findAllUnassignedToCourse(courseId);
    }

    /**
     * {@inheritDoc}
     *
     * Remove student with course, if the student is assigned to the course.
     *
     * @see StudentRepository
     * @see Transactional
     *
     * @throws IllegalArgumentException
     * @throws NullPointerException
     */
    @Override
    @Transactional
    public void removeStudentFromCourse(Long studentId, Long courseId) {
        Objects.requireNonNull(studentId, "Student id cannot be null.");
        Objects.requireNonNull(courseId, "Course id cannot be null");

        if (!studentRepository.isStudentAlreadyAssignedToCourse(studentId, courseId)) {
            throw new ServiceException("Course is already unassigned to student.");
        }

        studentRepository.removeStudentFromCourse(studentId, courseId);
    }

    /**
     * {@inheritDoc}
     *
     * @see StudentRepository
     */
    @Override
    public List<Student> getStudents(Long[] ids, Long[] excludeIds) {
        boolean isFindByIds = !(Objects.isNull(ids) || ids.length == 0);
        boolean isExcludeIds = !(Objects.isNull(excludeIds) || excludeIds.length == 0);
        if (isExcludeIds) {
            return studentRepository.findAllWithoutId(Arrays.asList(excludeIds));
        } else if (isFindByIds) {
            return (List<Student>) studentRepository.findAllById(Arrays.asList(ids));
        } else {
            return (List<Student>) studentRepository.findAll();
        }
    }

    /**
     * {@inheritDoc}
     *
     * Use studentRepository to find courses' id of student
     *
     * @see StudentRepository
     *
     * @throws NullPointerException
     */
    @Override
    public List<Long> getCoursesId(Long id) {
        Objects.requireNonNull(id, "Id cannot be null.");

        return studentRepository.findCoursesIdByStudentId(id);
    }
}
