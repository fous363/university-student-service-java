package kz.zhanbolat.student.service;

import kz.zhanbolat.student.entity.Student;

import java.util.List;

/**
 * A service interface for student logic
 */
public interface StudentService {
    /**
     * Get student by account id
     *
     * @param userId - account id
     * @return student
     */
    Student getStudentByUserId(Long userId);

    /**
     * Create student
     *
     * @param student - student
     * @return created student
     */
    Student addStudent(Student student);

    /**
     * Get students assigned to course
     *
     * @param courseId - course's id
     * @return list of students
     */
    List<Student> getStudentsAssignedToCourse(Long courseId);

    /**
     * Assign students to the course
     *
     * @param courseId - course's id
     * @param students - list of students
     */
    void assignStudentListToCourse(Long courseId, List<Student> students);

    /**
     * Get students, that are not assigned to course
     *
     * @param courseId - course's id
     * @return list of students
     */
    List<Student> getStudentsUnassignedToCourse(Long courseId);

    /**
     * Remove student with course
     *
     * @param studentId - student's id
     * @param courseId - course's id
     */
    void removeStudentFromCourse(Long studentId, Long courseId);

    /**
     * Get students' list. If ids is not null, then return list of students with these ids.
     * If excludeIds is not null, then return list of students without these ids.
     * If they both are null, then return all students list.
     *
     * @param ids - students' id
     * @param excludeIds - students' id to exclude
     * @return list of students
     */
    List<Student> getStudents(Long[] ids, Long[] excludeIds);

    /**
     * Get courses of student
     *
     * @param id - student's id
     * @return list of courses' id
     */
    List<Long> getCoursesId(Long id);
}
