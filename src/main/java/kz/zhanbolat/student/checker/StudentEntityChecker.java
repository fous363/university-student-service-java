package kz.zhanbolat.student.checker;

import kz.zhanbolat.student.entity.Student;
import kz.zhanbolat.student.util.StringUtil;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Student entity checker implementation
 */
@Component
public class StudentEntityChecker implements EntityChecker<Student> {

    /**
     * Check student on creating.
     *
     * @param student - student
     */
    @Override
    public void checkOnCreate(Student student) {
        Objects.requireNonNull(student);
        StringUtil.requiredNotEmpty(student.getFullName(), "Full name should not be empty");
        StringUtil.requiredNotEmpty(student.getEmail(), "Email should not be empty");
        StringUtil.requiredNotEmpty(student.getPhone(), "Phone should not be empty");
        Objects.requireNonNull(student.getBirthday(), "Birthday should be specified");
    }
}
