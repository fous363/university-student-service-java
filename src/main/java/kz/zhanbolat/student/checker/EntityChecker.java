package kz.zhanbolat.student.checker;

/**
 * Check the entity on creating
 *
 * @param <T> - entity class
 */
public interface EntityChecker<T> {
    /**
     * Check the entity object on creating event
     *
     * @param t - entity object
     */
    void checkOnCreate(T t);
}
