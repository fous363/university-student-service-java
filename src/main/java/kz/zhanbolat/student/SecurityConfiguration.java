package kz.zhanbolat.student;

import kz.zhanbolat.student.controller.filter.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthorizationFilter authorizationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin().disable()
                .logout().disable()
                .csrf().disable()
                .authorizeRequests(authorizeRequestsCustomizer -> authorizeRequestsCustomizer
                        .antMatchers(HttpMethod.GET, "/api/student/user/*",
                                "/api/student/*/courses/id").hasAuthority("STUDENT")
                        .antMatchers(HttpMethod.GET, "/api/student/assigned/course/*",
                                "/api/student/unassigned/course/*").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.POST, "/api/student/").permitAll()
                        .antMatchers(HttpMethod.POST, "/api/student/list/assign/course/*",
                                "/api/student/*/unassign/course/*").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.GET, "/api/student").hasAnyAuthority("ADMIN", "LECTURER", "STUDENT")
                        .and()
                        .addFilterBefore(jwtAuthorizationFilterBean().getFilter(), UsernamePasswordAuthenticationFilter.class))
                .cors().disable();
    }

    @Bean
    public FilterRegistrationBean jwtAuthorizationFilterBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(authorizationFilter);
        registration.addUrlPatterns("/api/student**");
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}
