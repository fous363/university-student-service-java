create table if not exists student(
    id bigserial primary key,
    full_name varchar(200) not null,
    phone char(16) not null,
    email varchar(100) not null,
    birthday date not null,
    user_id bigint not null
);