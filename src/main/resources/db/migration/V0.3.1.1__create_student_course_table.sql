create table if not exists student_course(
    student_id bigint not null,
    course_id bigint not null,
    foreign key (student_id) references student(id)
);