insert into student(full_name, email, phone, birthday, user_id)
values ('first_name last_name middle_name', 'test@test.test', '+7(777)777-77-77', '2021-2-24', 1);
insert into student(full_name, email, phone, birthday, user_id)
values ('Test student for course', 'test', 'test', '2021-4-10', 2);
insert into student(full_name, email, phone, birthday, user_id)
values ('Test unassigned student for course', 'test', 'test', now(), 3);
insert into student(full_name, email, phone, birthday, user_id)
values ('Test assigned student for course', 'test', 'test', '2021-4-10', 4);

insert into student_course(student_id, course_id) values (2, 1);
insert into student_course(student_id, course_id) values (2, 2);
insert into student_course(student_id, course_id) values (4, 1);
