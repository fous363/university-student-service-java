package kz.zhanbolat.student;

import kz.zhanbolat.student.entity.Student;
import kz.zhanbolat.student.exception.ServiceException;
import kz.zhanbolat.student.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import static kz.zhanbolat.student.TestConstant.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class StudentServiceImplTest {
    @Autowired
    private StudentService studentService;
    @Autowired
    private Function<Long, Student> studentGenerator;

    @Test
    public void givenNull_whenGetStudentByUserId_thenThrowException() {
        assertThrows(Exception.class, () -> studentService.getStudentByUserId(null));
    }

    @Test
    public void givenNotExistingUserId_whenGetStudentByUserId_thenThrowException() {
        assertThrows(ServiceException.class, () -> studentService.getStudentByUserId(NOT_EXISTING_STUDENT_ID));
    }

    @Test
    public void givenExistingUserId_whenGetStudentByUserId_thenReturnStudent() {
        final Student student = studentService.getStudentByUserId(USER_ID);

        assertEquals(USER_ID, student.getUserId());
        assertEquals(STUDENT_ID, student.getId());
        assertEquals(LocalDate.of(2021, 2, 24), student.getBirthday());
        assertEquals("first_name last_name middle_name", student.getFullName());
        assertEquals("test@test.test", student.getEmail());
        assertEquals("+7(777)777-77-77", student.getPhone());
    }

    @Test
    public void givenStudent_whenAddStudent_thenReturnStudentWithId() {
        Student student = new Student();
        student.setFullName("test");
        student.setEmail("test");
        student.setBirthday(LocalDate.now());
        student.setPhone("test");

        Student createdStudent = studentService.addStudent(student);

        assertNotNull(createdStudent.getId());
        assertEquals(student.getFullName(), createdStudent.getFullName());
        assertEquals(student.getEmail(), createdStudent.getEmail());
        assertEquals(student.getBirthday(), createdStudent.getBirthday());
        assertEquals(student.getPhone(), createdStudent.getPhone());
    }

    @Test
    public void givenNull_whenAddStudent_thenThrowException() {
        assertThrows(Exception.class, () -> studentService.addStudent(null));
    }

    @Test
    public void givenExistingStudentId_whenAddStudent_thenThrowException() {
        Student student = new Student();
        student.setId(1L);
        student.setFullName("test");
        student.setEmail("test");
        student.setPhone("test");
        student.setBirthday(LocalDate.now());

        assertThrows(ServiceException.class, () -> studentService.addStudent(student));
    }

    @Test
    public void givenNull_whenGetStudentsAssignedToCourse_thenThrowException() {
        assertThrows(Exception.class, () -> studentService.getStudentsAssignedToCourse(null));
    }

    @Test
    public void givenCourseId_whenGetStudentsAssignedToCourse_thenReturnNotEmptyList() {
        List<Student> students = studentService.getStudentsAssignedToCourse(COURSE_ID);

        assertNotNull(students);
        assertFalse(students.isEmpty());
    }

    @Test
    public void givenNull_whenAssignStudentListToCourse_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> studentService.assignStudentListToCourse(null, null));
            assertThrows(Exception.class, () -> studentService.assignStudentListToCourse(1L, null));
            assertThrows(Exception.class, () -> studentService.assignStudentListToCourse(null, Collections.singletonList(new Student())));
        });
    }

    @Test
    public void givenEmptyList_whenAssignStudentListToCourse_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> studentService.assignStudentListToCourse(COURSE_ID, Collections.emptyList()));
    }

    @Test
    public void givenStudentAlreadyAssignedToCourse_whenAssignStudentCourseList_thenThrowException() {
        List<Student> students = Arrays.asList(studentGenerator.apply(STUDENT_ID), studentGenerator.apply(ASSIGNED_STUDENT));

        assertThrows(ServiceException.class, () -> studentService.assignStudentListToCourse(COURSE_ID, students));
    }

    @Test
    public void givenStudentsAndCourse_whenAssignStudentListToCourse_thenDoesNotThrowException() {
        assertDoesNotThrow(() -> studentService.assignStudentListToCourse(COURSE_ID, Collections.singletonList(studentGenerator.apply(STUDENT_ID))));
    }

    @Test
    public void givenNull_whenGetStudentsUnassignedToCourse_thenThrowException() {
        assertThrows(Exception.class, () -> studentService.getStudentsUnassignedToCourse(null));
    }

    @Test
    public void givenCourseId_whenGetStudentsUnassignedToCourse_thenReturnNotEmptyList() {
        List<Student> students = studentService.getStudentsUnassignedToCourse(COURSE_ID);

        assertNotNull(students);
        assertFalse(students.isEmpty());
    }

    @Test
    public void givenNull_whenRemoveStudentFromCourse_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> studentService.removeStudentFromCourse(null, null));
            assertThrows(Exception.class, () -> studentService.removeStudentFromCourse(1L, null));
            assertThrows(Exception.class, () -> studentService.removeStudentFromCourse(null, 1L));
        });
    }

    @Test
    public void givenUnassignedStudentAndCourse_whenRemoveStudentFromCourse_thenThrowException() {
        assertThrows(ServiceException.class, () -> studentService.removeStudentFromCourse(STUDENT_WITHOUT_COURSE, COURSE_ID));
    }

    @Test
    public void givenStudentAndCourse_whenRemoveStudentFromCourse_thenDoesNotThrowException() {
        assertDoesNotThrow(() -> studentService.removeStudentFromCourse(STUDENT_WITH_COURSE, COURSE_ID));
    }

    @Test
    public void givenNoIds_whenGetStudents_thenReturnAllStudents() {
        List<Student> students = studentService.getStudents(null, null);

        assertNotNull(students);
        assertFalse(students.isEmpty());
        assertTrue(students.size() > 1);
    }

    @Test
    public void givenIds_whenGetStudents_thenReturnListOfOneElement() {
        List<Student> students = studentService.getStudents(new Long[]{STUDENT_ID}, null);

        assertNotNull(students);
        assertFalse(students.isEmpty());
        assertEquals(1, students.size());
    }

    @Test
    public void givenExcludeIds_whenGetStudents_thenReturnListWithoutExcludedStudent() {
        List<Student> students = studentService.getStudents(null, new Long[] {STUDENT_ID});

        assertNotNull(students);
        assertFalse(students.isEmpty());
        assertTrue(students.stream().noneMatch(student -> Objects.equals(student.getId(), STUDENT_ID)));
    }

    @Test
    public void givenNull_whenGetCoursesId_thenThrowException() {
        assertThrows(Exception.class, () -> studentService.getCoursesId(null));
    }

    @Test
    public void givenStudentId_whenGetCoursesId_thenReturnNotEmptyList() {
        List<Long> coursesId = studentService.getCoursesId(STUDENT_WITH_COURSE);

        assertNotNull(coursesId);
        assertFalse(coursesId.isEmpty());
    }
}
