package kz.zhanbolat.student;

public class TestConstant {
    public static final long STUDENT_ID = 1L; // use it on successful cases
    public static final long NOT_EXISTING_STUDENT_ID = 100L;
    public static final long USER_ID = 1L;
    public static final long COURSE_ID = 1L;
    public static final long STUDENT_WITH_COURSE = 2L;
    public static final long STUDENT_WITHOUT_COURSE = 3L;
    public static final long ASSIGNED_STUDENT = 4L;
}
