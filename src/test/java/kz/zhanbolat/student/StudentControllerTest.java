package kz.zhanbolat.student;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.zhanbolat.student.entity.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static kz.zhanbolat.student.TestConstant.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class StudentControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private Function<Long, Student> studentGenerator;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @WithMockUser(username = "test_student", authorities = {"STUDENT"})
    public void givenNotExistingUserId_whenGetStudentByUserId_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/student/user/" + NOT_EXISTING_STUDENT_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(username = "test_student", authorities = {"STUDENT"})
    public void givenExistingUserId_whenGetStudentByUserId_thenReturnStudent() throws Exception {
        mockMvc.perform(get("/api/student/user/" + STUDENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.fullName").value("first_name last_name middle_name"))
                .andExpect(jsonPath("$.email").value("test@test.test"))
                .andExpect(jsonPath("$.phone").value("+7(777)777-77-77"))
                .andExpect(jsonPath("$.birthday").value(LocalDate.of(2021, 2, 24).toString()))
                .andExpect(jsonPath("$.userId").value(1));
    }

    @Test
    public void givenStudent_whenAddStudent_thenReturnStudent() throws Exception {
        LocalDate now = LocalDate.now();
        Student student = new Student();
        student.setFullName("test");
        student.setEmail("test");
        student.setBirthday(now);
        student.setPhone("test");

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(post("/api/student/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(student)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.fullName").value(student.getFullName()))
                .andExpect(jsonPath("$.email").value(student.getEmail()))
                .andExpect(jsonPath("$.phone").value(student.getPhone()))
                .andExpect(jsonPath("$.birthday").value(now.toString()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenCourseId_whenGetStudentsAssignedToCourse_thenReturnNotEmptyList() throws Exception {
        mockMvc.perform(get("/api/student/assigned/course/" + COURSE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenStudentsAlreadyAssignedToCourse_whenAssignStudentListToCourse_thenReturnErrorResponse() throws Exception {
        List<Student> students = Arrays.asList(studentGenerator.apply(STUDENT_ID), studentGenerator.apply(STUDENT_WITH_COURSE));

        String payload = objectMapper.writeValueAsString(students);

        mockMvc.perform(post("/api/student/list/assign/course/" + COURSE_ID)
                        .contentType(MediaType.APPLICATION_JSON).content(payload))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenStudentsAndCourse_whenAssignStudentListToCourse_thenReturnStatusOk() throws Exception {
        String payload = objectMapper.writeValueAsString(Collections.singletonList(studentGenerator.apply(STUDENT_ID)));

        mockMvc.perform(post("/api/student/list/assign/course/" + COURSE_ID)
                .contentType(MediaType.APPLICATION_JSON).content(payload))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenCourseId_whenGetStudentsUnassignedToCourse_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/student/unassigned/course/" + COURSE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenUnassignedStudentAndCourse_whenRemoveCourseFromStudent_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(delete("/api/student/" + STUDENT_WITHOUT_COURSE + "/remove/course/" + COURSE_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenStudentAndCourse_whenRemoveCourseFromStudent_thenReturnStatusOk() throws Exception {
        mockMvc.perform(delete("/api/student/" + ASSIGNED_STUDENT + "/remove/course/" + COURSE_ID))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenRequest_whenGetStudents_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/student"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenIds_whenGetStudents_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/student?ids=" + STUDENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN"})
    public void givenExcludeIds_whenGetStudents_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/student?exclude-ids=" + STUDENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    public void givenStudentId_whenGetCoursesId_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/student/" + STUDENT_WITH_COURSE + "/courses/id"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}
