package kz.zhanbolat.student;

import kz.zhanbolat.student.checker.StudentEntityChecker;
import kz.zhanbolat.student.entity.Student;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StudentEntityCheckerTest {
    private StudentEntityChecker studentEntityChecker;

    @BeforeEach
    public void init() {
        studentEntityChecker = new StudentEntityChecker();
    }

    @Test
    public void givenNull_whenCheckOnCreate_thenThrowException() {
        assertThrows(Exception.class, () -> studentEntityChecker.checkOnCreate(null));
    }

    @Test
    public void givenEmptyFullName_whenCheckOnCreate_thenThrowException() {
        Student student = new Student();
        student.setFullName("");
        student.setBirthday(LocalDate.MAX);
        student.setPhone("test");
        student.setEmail("test");

        assertThrows(IllegalArgumentException.class, () -> studentEntityChecker.checkOnCreate(student));
    }

    @Test
    public void givenEmptyPhone_whenCheckOnCreate_thenThrowException() {
        Student student = new Student();
        student.setFullName("test");
        student.setBirthday(LocalDate.MAX);
        student.setPhone("");
        student.setEmail("test");

        assertThrows(IllegalArgumentException.class, () -> studentEntityChecker.checkOnCreate(student));
    }

    @Test
    public void givenEmptyEmail_whenCheckOnCreate_thenThrowException() {
        Student student = new Student();
        student.setFullName("test");
        student.setBirthday(LocalDate.MAX);
        student.setPhone("test");
        student.setEmail("");

        assertThrows(IllegalArgumentException.class, () -> studentEntityChecker.checkOnCreate(student));
    }

    @Test
    public void givenNullBirthday_whenCheckOnCreate_thenThrowException() {
        Student student = new Student();
        student.setFullName("test");
        student.setPhone("test");
        student.setEmail("test");

        assertThrows(Exception.class, () -> studentEntityChecker.checkOnCreate(student));
    }

    @Test
    public void givenCorrectStudent_whenCheckOnCreate_thenDoesNotThrowException() {
        Student student = new Student();
        student.setFullName("test");
        student.setBirthday(LocalDate.MAX);
        student.setPhone("test");
        student.setEmail("test");

        assertDoesNotThrow(() -> studentEntityChecker.checkOnCreate(student));
    }
}
